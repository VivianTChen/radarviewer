#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtDebug>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    //set parent window bottomLeftX, bottomLeftY
    if (argc > 2 ) {
        int bottomLeftX = atoi(argv[1]);
        int bottomLeftY = atoi(argv[2]);
        qDebug() << "pass parameters: " << bottomLeftX << " and " << bottomLeftY;
        engine.rootContext()->setContextProperty("bottomLeftX", bottomLeftX);
        engine.rootContext()->setContextProperty("bottomLeftY", bottomLeftY);
      }
    else {
        engine.rootContext()->setContextProperty("bottomLeftX", nullptr);
        engine.rootContext()->setContextProperty("bottomLeftY", nullptr);
    }

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}

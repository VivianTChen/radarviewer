import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import QtQuick.Controls 2.4

ApplicationWindow {
    id: radarWindow
    visible: true
    width: 200
    height: 100
    title: qsTr("Radar Viewer")
    flags: Qt.FramelessWindowHint | Qt.Window
    color: "transparent"

    onActiveChanged: {
        if(!active) {
           console.debug(" radar application inactive....")
        }
    }

    property int previousX
    property int previousY
    property color displayColor : "yellow"
    property real space: height/4
    property real fontDisplay: height/6

    Rectangle {
        anchors.fill: parent
        border.color: displayColor
        border.width: 2
        radius: space
        color: "transparent"

        ColumnLayout {
            RowLayout {
                Label {
                    topPadding: space * 0.6
                    leftPadding: space
                    Layout.minimumWidth: 3*space
                    text: "Target"
                    font.pixelSize: fontDisplay
                    color: displayColor
                }
                Label {
                    topPadding: space * 0.6
                    leftPadding: space
                    text: "54"
                    font.pixelSize: fontDisplay
                    font.bold: true
                    color: displayColor
                }
            }

            RowLayout {
                Label {
                    leftPadding: space
                    Layout.minimumWidth: 3*space
                    text: "Fast"
                    font.pixelSize: fontDisplay
                    color: displayColor
                }
                Label {
                    leftPadding: space
                    text: "63"
                    font.pixelSize: fontDisplay
                    font.bold: true
                    color: displayColor
                }
            }

            RowLayout {
                Label {
                    leftPadding: space
                    Layout.minimumWidth: 3*space
                    text: "Patrol"
                    font.pixelSize: fontDisplay
                    color: displayColor
                }
                Label {
                    leftPadding: space
                    text: "57"
                    font.pixelSize: fontDisplay
                    font.bold: true
                    color: displayColor
                }
            }
        }
    }

    //for move application window
    MouseArea {
        id: windowDragMouseArea
        anchors.fill: parent
        //anchors.left: parent.left
        //anchors.right: parent.right
        //width: parent.width
        //height: 50

        onPressed: {
            previousX = mouseX
            previousY = mouseY
            elapsedTimer.stop()
            console.debug("elapsedTimer stop...")
        }

        onMouseXChanged: {
            var dx = mouseX - previousX
            radarWindow.setX(radarWindow.x + dx)
        }

        onMouseYChanged: {
            var dy = mouseY - previousY
            radarWindow.setY(radarWindow.y + dy)
        }

        onReleased: {
            elapsedTimer.start();
            console.debug("elapsedTimer start ...")
        }

    }


    Timer  {
            id: elapsedTimer
            interval: 20000;
            running: true;
            repeat: false;
            onTriggered: Qt.quit()
        }

    Component.onCompleted: {
        if (bottomLeftX && bottomLeftY)
        {
            setX(bottomLeftX + 20);
            setY(bottomLeftY - height - 20);
        }
    }


}
